# COPYRIGHT #
Copyright (c) 2013-2014, Indian Institute of Technology Madras (IIT Madras)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation 
and/or other materials provided with the distribution.

Neither the name of IIT Madras nor the names of its contributors may be 
used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
THE POSSIBILITY OF SUCH DAMAGE.

# README #

This repository has the results from AES SBox implementations with different composite fields.
There are two files: forward.pdf and inverse.pdf.
The Forward.pdf lists the post-implementation results for the forward SBox.
The Inverse.pdf lists the post-implementation results for the inverse SBox.


### Interpreting the Files ###
Each pdf file lists the LUT requirements from 2880 implementations and the number of 6-input LUTs required 
post-implementation on synthesising on Vivado 2017.1 for an Artix-7 FPGA. A timing file 
was used to control the frequency and critical time. The implementations were synthesized 
at 25MHz,50MHz and 67MHz. The forward and inverse implementations were stored in two seperate pdfs.

Each entry is of the form Q(z)_t_v_g1_g2"

Q(z) has values 19, 25, or 31; each representing an irreducible polynomial in GF(2^4).
In particular, 19 represents x^4+x+1, 25 represent x^4+x^3+1, and 31 represents x^4+x^3+x^2+x+1.

P(y)=y^2+ty+v :Another irreducible used to construct the composite field GF((2^4)^2)

g1 is the generator in this extension field GF((2^4)^2)

g2 is the generator in the AES field of GF(2^8) with irreducible polynomial x^8+x^4+x^3+x+1

The best implementation for the forward and inverse AES SBox is provided in the two verilog files - AES_SBox_19_1_13_16_31.v and AES_InvSbox_19_1_13_16_31.v


### Reference Paper ###
Use this paper to cite the work and have more details about the implementation:

Aditya Pradeep, Vishal Mohanty, Adarsh Muthuveeru Subramaniam, and Chester Rebeiro, 'Revisiting AES SBox Composite Field Implementations for FPGAs', 
IEEE Embedded Systems Letters, https://ieeexplore.ieee.org/document/8641387.
